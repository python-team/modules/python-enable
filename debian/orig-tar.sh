#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=enthought-chaco2-$2.orig

# cleanup the upstream sources
tar zxf $3
mv enable-$2 $DIR

# remove freetype2 source from enthought.kiva
(cd $DIR; rm -rf kiva/agg/freetype2)

# remove fonttools sources
(cd $DIR; rm -rf kiva/fonttools/fontTools)

# remove non-free gpc sources
(cd $DIR; rm -rf kiva/agg/agg-24/gpc)

# create the tarball
GZIP=--best tar -c -z -f $3 $DIR
rm -rf $DIR
