# THIS FILE IS GENERATED FROM ENABLE SETUP.PY

#: The full version of the package, including a development suffix
#: for unreleased versions of the package.
version = "5.2.1"

#: The full version of the package, same as 'version'
#: Kept for backward compatibility
full_version = version

#: The Git revision from which this release was made.
git_revision = "Unknown"

#: Flag whether this is a final release
is_released = True
